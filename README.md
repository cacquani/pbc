# Play By Chat educational project

## Goal

Create a Play By Chat engine and repeat the exact same project in different
programming languages.

This will highlight differences in grammar, syntax, performance, easiness of
testing and other parameters.

This document will collect the list of functionalities for future
implementations.

## Structure

* Pre-commit hook

* Code coverage

* BDD suite (possibly human-readable)

* TDD suite

* Authentication

* Authorization

* Deployment

* Hot reload

* CI integration (CircleCI)

### Stuff to experiment

* Drag and Drop
** Maps/Art/Music upload
** Move around bits of map on a grid

* Push notifications

* Use Kafka/message queues to manage the chat rooms. One room, one topic.
  Clients subscribe to the topic related to the room they're in.

## Languages/frameworks

### Implemented

* [Ruby on Rails](./rails/README.md)

### Wishlist

* Ruby on Rake with ROM-RB
  https://github.com/rom-rb/rom
  http://rom-rb.org/learn/introduction/why/
  http://rom-rb.org/learn/repositories/quick-start/
  http://rom-rb.org/3.0/learn/getting-started/rails-setup/

* Javascript (framework TBD)

* Java (framework TBD)

* Elixir on Phoenix

* Django on Python

* Crystal
  https://crystal-lang.org/docs/
