# frozen_string_literal: true

# Create the Roles table
class CreateRoles < ActiveRecord::Migration[5.1]
  def change
    create_table :roles do |t|
      t.string :name, nil: false, unique: true

      t.timestamps
    end
  end
end
