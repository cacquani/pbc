# frozen_string_literal: true

# Create the Player table
class CreatePlayers < ActiveRecord::Migration[5.0]
  def change
    create_table :players do |t|
      t.references :user
      t.string :username, unique: true, nil: false
      t.text :biography

      t.timestamps
    end
  end
end
