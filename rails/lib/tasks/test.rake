# frozen_string_literal: true

env = ENV['RAILS_ENV'] || ENV['RAKE_ENV']

unless env == 'production'
  require 'rubocop/rake_task'

  RuboCop::RakeTask.new

  task default: [
    'db:migrate', :rubocop, :cucumber, 'db:truncate'
  ]
end
