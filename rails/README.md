# README

Play by chat educational project in Ruby on Rails

## How to run

### Setup

#### System requirements and external services:

* Ruby 2.4.0 (suggested rvm or rbenv)

* Node (suggested nvm)

* Database: Lightweight SQLite for test and development.
  To be exchanged with a PostgreSQL for an eventual deploy to live.

* Server: TBD

* Services (job queues, cache servers, search engines, etc.) TBC

#### Configuration

* Database creation

* Database initialization

### Start the server locally

`> bundle exec rackup`

### Run the test suite

Available tests:

* Linting (Rubocop)

* Unit tests (RSpec)

* Behavioural tests (Cucumber)

### Deployment

Deployment instructions

## Features

-[x] Code coverage

-[x] BDD suite

-[x] TDD suite

-[x] Authentication

-[x] Clean styling using pure https://purecss.io

-[x] Adapt the upper menu on whether logged in or not

-[x] Authorization
     Three roles: player, storyteller, deity

## ToDo

* Use Kafka for the chat:
  * Each chat is a topic
  * Each instance will subscribe to the chat topic he's in, and comsume from
    said topic

* Keep the games logs in Kafka or some other adequate non-database

* Implement at least one of the common, free game mechanics
  * Add IRC-style dice rolling macros for public dice rolling.
  * Add a macro that will let both the storyteller and the player (but no-one
    else) see the result of the dice roll.
  * Add more game mechanics

* Logging to weird things:
** Relational Database
** NoSQL database
** Kafka
** http://www.rsyslog.com/doc/v8-stable/tutorials/high_database_rate.html
   http://www.rsyslog.com/guides-for-rsyslog/
   https://stackoverflow.com/questions/10794602/using-rsyslogd-in-rails-3-0#10803867
   http://guides.rubyonrails.org/debugging_rails_applications.html#the-logger

* Add audit/version control
  https://github.com/airblade/paper_trail
  https://github.com/ActsAsParanoid/acts_as_paranoid
  https://github.com/collectiveidea/audited

* Add a My Account page

* Create the dashbard

* Add some more graphics

* Add player data

* Add character data

* Create a global live chat functionality

* Limit the 'list user' to 'Storyteller' and 'Deity' role only

* Add Game Worlds (define)

* Add Campaign (define) and Session (define)

* Consider multitenancy
** https://www.allerin.com/blog/multi-tenancy-in-rails/
** https://samuel.kadolph.com/2010/12/simple-rails-multi-tenancy/
** https://github.com/jekuno/milia
** https://github.com/ErwinM/acts_as_tenant
** https://github.com/influitive/apartment
** https://gorails.com/episodes/multitenancy-with-apartment

* Other points:
** Direction of the conversation (chat classic, pyrosia or configurable?)
** World maps?
** Schedule sessions and reminders?
** Integration with email and chats (slack etc)
** Possibility of remote 1-1 consultation/private rooms?
** Audio integration
** Camera integration
