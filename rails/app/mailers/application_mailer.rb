# frozen_string_literal: true

# Base ApplicationMailer layer
class ApplicationMailer < ActionMailer::Base
  default from: 'from@example.com'
  layout 'mailer'
end
