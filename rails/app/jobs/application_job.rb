# frozen_string_literal: true

# Base ApplicationJob layer
class ApplicationJob < ActiveJob::Base
end
