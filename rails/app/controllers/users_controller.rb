# frozen_string_literal: true

# UsersController
# GET show: the user's own homepage
# GET index: a list of users
class UsersController < ApplicationController
  before_action :authorize_action, except: [:show]
  load_and_authorize_resource only: [:show]

  def index; end

  def show
    user = find_user
    render :show, locals: { user: user }
  end

  private

  def find_user
    @user = User.find(params[:id])
  end
end
