# frozen_string_literal: true

# Base ApplicationController
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!

  rescue_from CanCan::AccessDenied do |exception|
    url = current_user.new_record? ? root_url : user_url(current_user)
    respond_to do |format|
      format.json { head :forbidden }
      format.html { redirect_to url, alert: exception.message }
    end
  end

  def after_sign_in_path_for(user)
    user_url(user)
  end

  def authorize_action
    authorize!(params['action'].to_sym, params['controller'].to_sym)
  end
end
