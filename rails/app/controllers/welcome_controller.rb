# frozen_string_literal: true

# Welcome page
class WelcomeController < ApplicationController
  skip_before_action :authenticate_user!
  before_action :authorize_action

  def index
    render :index
  end
end
