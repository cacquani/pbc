# frozen_string_literal: true

# CanCan ability
class Ability
  include CanCan::Ability

  def initialize(user = nil)
    user ||= User.new # for guest
    can :index, :welcome
    return if user.new_record?
    can :read, User, id: user.id
    return unless user.superuser?
    can :read, User
    return unless user.role?(:deity)
    can :index, :users
  end
end
