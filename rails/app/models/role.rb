# frozen_string_literal: true

# Roles for users
class Role < ApplicationRecord
  ROLES = %w[player storyteller deity].freeze

  validates :name, presence: true, uniqueness: { case_sensitive: false }
  validates :name, inclusion: {
    in: ROLES,
    message: '"%{value}" is not a valid role',
    case_sensitive: false
  }

  has_many :assignments
  has_many :users, through: :assignments

  def self.superuser
    ROLES[1, 2]
  end
end
