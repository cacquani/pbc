# frozen_string_literal: true

# It's pretty common for folks to monkey patch ActiveRecord::Base to
# work around an issue or introduce extra functionality. Instead of
# shoving even more stuff in ActiveRecord::Base, ApplicationRecord can
# hold all those custom work the apps may need.
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
