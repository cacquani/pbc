# frozen_string_literal: true

# Player record, will hold all player information
class Player < ApplicationRecord
  belongs_to :user

  validates :username, presence: true, uniqueness: true
end
