# frozen_string_literal: true

# User/Role relation
class Assignment < ApplicationRecord
  belongs_to :user
  belongs_to :role
end
