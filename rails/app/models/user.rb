# frozen_string_literal: true

# Log in user.
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :omniauthable
  devise :database_authenticatable, :confirmable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :timeoutable, :lockable

  has_one :player
  has_many :assignments
  has_many :roles, through: :assignments

  def username
    player ? player.username : email
  end

  def role?(role)
    roles.any? { |r| r.name.underscore == role.to_s }
  end

  def superuser?
    (Role.superuser & roles.map(&:name)).present?
  end
end
