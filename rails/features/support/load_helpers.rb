# frozen_string_literal: true

require './features/support/paths'
require './features/support/database'

World(NavigationHelpers)
World(DatabaseHelpers)
