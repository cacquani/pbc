# frozen_string_literal: true

# Map rails paths to feature definitions
module NavigationHelpers
  def path_to(page)
    case page
    when /^the public home$/
      root_url
    when /^my personal home$/
      user_path(1)
    when /^the users list$/
      users_path
    else
      raise "Can't find mapping from \"#{page}\" to a path."
    end
  end
end
