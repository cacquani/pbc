# frozen_string_literal: true

# Helper methods to seed the test database
module DatabaseHelpers
  USERS = [
    ['nynaeve.almeara@tworivers.ch', 'Wise Woman', 'Nynaeve', 'Al\'Meara',
     'storyteller'],
    ['egwene.alvere@tworivers.ch', 'Egwene', 'Egwene', 'Al\'Vere', 'player'],
    ['moraine.damodred@cairhien.ch', 'Moraine Sedai', 'Moraine', 'Damodred',
     'deity']
  ].freeze

  def self.seed_users
    USERS.each do |data|
      user = create_user(data)
      create_player(user, data)
      create_role(user, data.last)
    end
  end

  def self.create_user(data)
    user = User.create!(
      email: data[0],
      password: 'password',
      firstname: data[2],
      lastname: data[3]
    )
    user.confirm
    user
  end

  def self.create_player(user, data)
    Player.create!(
      user: user,
      username: data[1]
    )
  end

  def self.create_role(user, role)
    Role.create!(
      name: role,
      users: [user]
    )
  end
end
