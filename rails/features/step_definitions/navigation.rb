# frozen_string_literal: true

require_relative '../support/load_helpers'

When(/^I navigate to (.+) page$/) do |name|
  visit path_to(name)
end

When(/^I click on the (.+) (link|button)$/) do |element, type|
  if type == 'link'
    click_link element
  else
    click_button element
  end
end
