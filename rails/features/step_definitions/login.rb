# frozen_string_literal: true

Given(/^I am(:? not)? logged in(?: as (.+))?$/) do |negate, user|
  unless negate
    DatabaseHelpers.seed_users
    visit '/users/sign_in'
    fill_in 'user_email', with: user
    fill_in 'user_password', with: 'password'
    click_button 'Log in'
  end
end

Given(/^my role is (.+)$/) do |role|
  # Just an explanation for the feature reader, the role is seeded with the user
end
