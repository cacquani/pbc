# frozen_string_literal: true

require 'rspec/expectations'

Then(/^I will see the public home page$/) do
  expect(page).to have_title('Play By Chat')
  expect(page).to have_content('Ready for adventure?')
end

Then(/^I will see the log in page$/) do
  expect(page).to have_selector('input#user_email')
  expect(page).to have_selector('input#user_password')
  expect(page).to have_selector('input[name="commit"][value="Log in"]')
end

Then(/^I will (?:see|be redirected to) my personal home page$/) do
  expect(page).to have_content('Welcome')
end

Then(/^I will see the Personal Data section$/) do
  expect(page).to have_selector('div.section', text: 'Personal Data')
end

Then(/^I will see my ((?:\w|\s)+) as (.+)$/) do |data, value|
  expect(page).to have_content("#{data.capitalize}:")
  expect(page).to have_content(value)
end

Then(/^I will (not)? ?see the navigation (.+) link$/) do |negation, link|
  if negation
    expect(page.find('nav.pure-menu')).not_to have_content(link)
  else
    expect(page.find('nav.pure-menu')).to have_content(link)
  end
end

Then(/^I will (not)? ?see the navigation welcome message$/) do |negation|
  if negation
    expect(page.find('nav.pure-menu')).not_to have_content('Hello Wise Woman')
  else
    expect(page.find('nav.pure-menu')).to have_content('Hello Wise Woman')
  end
end

Then(/^I will see all the users$/) do
  expect(page.find('h2')).to have_content('Users')
end
