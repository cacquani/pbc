Feature: Player account
  As a player
  I can't see the other users

  Background:
    Given I am logged in as egwene.alvere@tworivers.ch
    And my role is player
    When I navigate to the users list page

  Scenario:
    Then I will be redirected to my personal home page
