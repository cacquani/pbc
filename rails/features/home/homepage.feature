Feature: Home page
  I can see the public home page

  Scenario: I can see the public home page
    Given I am not logged in
    When I navigate to the public home page
    Then I will see the public home page

