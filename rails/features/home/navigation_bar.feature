Feature: navigation bar
  The navigation bar content changes
  based on whether I am logged in or not

  Scenario: I am not logged in
    Given I am not logged in
    When I navigate to the public home page
    Then I will see the navigation Log in link
    And I will see the navigation Sign up link
    And I will not see the navigation welcome message
    And I will not see the navigation My Account link

  Scenario: I am logged in
    Given I am logged in as nynaeve.almeara@tworivers.ch
    When I navigate to the public home page
    Then I will not see the navigation Log in link
    And I will not see the navigation Sign up link
    And I will see the navigation welcome message
    And I will see the navigation My Account link

  Scenario: I am logged in and I log out
    Given I am logged in as nynaeve.almeara@tworivers.ch
    When I navigate to the public home page
    And I click on the Log out button
    Then I will see the navigation Log in link
    And I will see the navigation Sign up link
    And I will not see the navigation welcome message
    And I will not see the navigation My Account link
