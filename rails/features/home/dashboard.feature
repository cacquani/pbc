Feature: Dashboard
  On my personal dashboard
  I see an overview of my information

  Background:
    Given I am logged in as nynaeve.almeara@tworivers.ch
    When I navigate to my personal home page

  Scenario: I will see my personal data
    Then I will see the Personal Data section
    And I will see my first name as Nynaeve
    And I will see my last name as Al'Meara
    And I will see my email as nynaeve.almeara@tworivers.ch
