Feature: Deity account
  As a deity
  I can see the other users

  Background:
    Given I am logged in as moraine.damodred@cairhien.ch
    And my role is deity
    When I navigate to the users list page

  Scenario:
    Then I will see all the users

