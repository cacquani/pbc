Feature: login
  In order to view my personal home page
  I am required to log in

  Scenario: I am not logged in
    Given I am not logged in
    When I navigate to my personal home page
    Then I will see the log in page

  Scenario: I am logged in
    Given I am logged in as nynaeve.almeara@tworivers.ch
    When I navigate to my personal home page
    Then I will see my personal home page
