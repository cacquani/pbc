# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Assignment, type: :model do
  context 'relations' do
    let(:assignment) { Assignment.new }

    it 'has one user' do
      expect(assignment.respond_to?(:user)).to be true
      expect(assignment.user).to be nil
    end

    it 'has one role' do
      expect(assignment.respond_to?(:role)).to be true
      expect(assignment.role).to be nil
    end
  end
end
