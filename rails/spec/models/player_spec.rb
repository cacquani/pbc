# frozen_string_literal: true

describe Player, type: :model do
  let(:user) do
    user = User.create!(
      email: 'nynaeve.almeara@tworivers.ch',
      password: 'password'
    )
    user.confirm
    user
  end

  context '#new' do
    context 'without params' do
      let(:player) { Player.new }

      it 'will fail validation' do
        error = 'Validation failed: User must exist, Username can\'t be blank'
        expect { player.save! }
          .to raise_error ActiveRecord::RecordInvalid, error
      end
    end

    context 'with duplicated username' do
      let!(:player) { Player.create(user: user, username: 'Username') }
      let(:player2) { Player.new(user: user, username: 'Username') }

      it 'will fail validation' do
        error = 'Validation failed: Username has already been taken'
        expect { player2.save! }
          .to raise_error ActiveRecord::RecordInvalid, error
      end
    end
  end
end
