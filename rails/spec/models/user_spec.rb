# frozen_string_literal: true

describe User, type: :model do
  context 'relations' do
    let(:user) { User.new }

    it 'has one player' do
      expect(user.respond_to?(:player)).to be true
      expect(user.player).to be nil
    end

    it 'has several roles' do
      expect(user.respond_to?(:roles)).to be true
      expect(user.roles).to eq []
    end
  end

  context '#username' do
    context 'when no player is associated' do
      let(:user) do
        user = User.create!(
          email: 'nynaeve.almeara@tworivers.ch',
          password: 'password'
        )
        user.confirm
        user
      end

      it 'will use the e-mail' do
        expect(user.username).to eq(user.email)
      end
    end

    context 'when a player is associated' do
      let(:user) do
        user = User.create!(
          email: 'egwene.alvere@tworivers.ch',
          password: 'password'
        )
        user.confirm
        Player.create!(user: user, username: 'Egwene Sedai')
        user
      end

      it 'will use the player\'s username' do
        expect(user.username).to eq('Egwene Sedai')
      end
    end
  end

  context '#role?' do
    let(:deity) { Role.create(name: 'deity') }
    let(:storyteller) { Role.create(name: 'storyteller') }
    let(:player) { Role.create(name: 'player') }
    let(:user) do
      user = User.create!(
        email: 'nynaeve.almeara@tworivers.ch',
        password: 'password'
      )
      user.confirm
      user.roles << storyteller
      user.roles << player
      user
    end

    it 'will complain when no argument passed' do
      expect { user.role? }.to raise_error ArgumentError
    end

    it 'will respond correctly' do
      expect(user.role?(:deity)).to be false
      expect(user.role?(:storyteller)).to be true
      expect(user.role?(:player)).to be true
    end
  end
end
