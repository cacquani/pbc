# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Ability, type: :model do
  let(:user) { User.new }
  let(:egwene) do
    egwene = User.create!(
      email: 'egwene.alvere@tworivers.ch',
      password: 'password'
    )
    egwene.confirm
    egwene
  end
  let(:nynaeve) do
    nynaeve = User.create!(
      email: 'nynaeve.almeara@tworivers.ch',
      password: 'password'
    )
    nynaeve.confirm
    nynaeve
  end
  let(:moraine) do
    moraine = User.create!(
      email: 'moraine.sedai@whitetower.ch',
      password: 'password'
    )
    moraine.confirm
    moraine
  end

  context 'roles' do
    context 'a guest' do
      let(:ability) { Ability.new(user) }

      it 'will be able to see (read) the welcome page' do
        expect(ability.can?(:index, :welcome)).to be true
      end

      it 'will not be able to see (read) a user page' do
        expect(ability.can?(:read, egwene)).to be false
      end

      it 'will not be able to list the users' do
        expect(ability.can?(:index, :users)).to be false
      end
    end

    context 'a player' do
      let(:ability) { Ability.new(egwene) }
      let!(:role) { Role.new(name: 'player', users: [egwene]) }

      it 'will be able to see (read) the welcome page' do
        expect(ability.can?(:index, :welcome)).to be true
      end

      it 'will be able to see (read) its own user page' do
        expect(ability.can?(:read, egwene)).to be true
      end

      it 'will not be able to see (read) another user page' do
        expect(ability.can?(:read, nynaeve)).to be false
      end

      it 'will not be able to list the users' do
        expect(ability.can?(:index, :users)).to be false
      end
    end

    context 'a storyteller' do
      let(:ability) { Ability.new(nynaeve) }
      let!(:role) { Role.create!(name: 'storyteller', users: [nynaeve]) }

      it 'will be able to see (read) the welcome page' do
        expect(ability.can?(:index, :welcome)).to be true
      end

      it 'will be able to see (read) its own user page' do
        expect(ability.can?(:read, nynaeve)).to be true
      end

      it 'will be able to see (read) another user page' do
        expect(ability.can?(:read, egwene)).to be true
      end

      it 'will not be able to list the users' do
        expect(ability.can?(:index, :users)).to be false
      end
    end

    context 'a deity' do
      let(:ability) { Ability.new(moraine) }
      let!(:role) { Role.create!(name: 'deity', users: [moraine]) }

      it 'will be able to see (read) the welcome page' do
        expect(ability.can?(:index, :welcome)).to be true
      end

      it 'will be able to see (read) its own user page' do
        expect(ability.can?(:read, moraine)).to be true
      end

      it 'will be able to see (read) another user page' do
        expect(ability.can?(:read, egwene)).to be true
      end

      it 'will be able to list the users' do
        expect(ability.can?(:index, :users)).to be true
      end
    end
  end
end
