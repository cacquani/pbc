# frozen_string_literal: true

describe Role, type: :model do
  context 'relations' do
    let(:role) { Role.new }

    it 'has several users' do
      expect(role.respond_to?(:users)).to be true
      expect(role.users).to eq []
    end
  end

  context '#new' do
    context 'with no name' do
      let(:role) { Role.new }

      it 'will fail validation' do
        expected_error = { name: [
          'can\'t be blank',
          '"" is not a valid role'
        ] }
        expect(role.valid?).to be false
        expect(role.errors.messages).to eq(expected_error)
      end
    end

    context 'with duplicated name' do
      let!(:role) { Role.create(name: 'deity') }
      let(:role2) { Role.new(name: 'deity') }

      it 'will fail validation' do
        expected_error = { name: ['has already been taken'] }
        expect(role2.valid?).to be false
        expect(role2.errors.messages).to eq(expected_error)
      end
    end

    context 'with a name not in the list' do
      let(:role) { Role.create(name: 'emperor') }

      it 'will fail validation' do
        expected_error = { name: ['"emperor" is not a valid role'] }
        expect(role.valid?).to be false
        expect(role.errors.messages).to eq(expected_error)
      end
    end
  end
end
