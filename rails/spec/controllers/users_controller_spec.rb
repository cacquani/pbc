# frozen_string_literal: true

require 'rails_helper'
# Test for UsersController
describe UsersController, type: :controller do
  include Devise::Test::ControllerHelpers

  describe 'GET #index' do
    it 'should get index' do
      get :index
      # Gets redirected by devise
      assert_response :found
    end
  end

  describe 'GET #SHOW' do
    it 'should get show' do
      get :show, params: { id: 1 }
      # Gets redirected by devise
      assert_response :found
    end
  end
end
