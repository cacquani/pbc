# frozen_string_literal: true

require 'rails_helper'

# Test for WelcomeController
describe WelcomeController, type: :controller do
  describe 'GET #index' do
    it 'should get index' do
      get :index
      assert_response :success
    end
  end
end
